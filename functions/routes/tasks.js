const { db } = require('../util/admin');

exports.getTodoFromProject = (req, res) => {
    let todoData = {};

    db
        .doc(`/projects/${req.params.projectId}`)
        .get()
        .then((doc) => {
            if (!doc.exists) {
                return res.status(404)
                    .json({ error: `Project ${req.params.projectId} not found` });
            }
            projectData = doc.data();
            projectData.projectId = doc.id;
            
            return db.collection('todos')
                .where('projectId', '==', req.params.projectId)
                .get();
        })
        .then(data => {
            todoData.todo = [];
            data.forEach(doc => {
                todoData.todo.push(doc.data());
            });
            return res.json(todoData);
        })
        .catch((err) => {
            res.status(500).json({ error: err.code });
            console.error(err);
        });
};

exports.editTodo = (req, res) => {
    const document = db.doc(`/todos/${req.params.todoId}`);

    const editTodo = {
        name: req.body.name
    }

    if (editTodo == '') {
        res.status(400).json({ error: 'Must not be empty' });
    }

    document
        .get()
        .then(doc => {
            if (!doc.exists) {
                return res.status(404)
                    .json({ error: `Todo ${req.params.todoId} not found` });
            } else {
                return document.update(editTodo);
            }
        })
        .then(() => {
            res.json({ message: `Todo ${req.params.todoId} edited successfully` })
        })
        .catch((err) => {
            console.error(err);
            return res.status(500).json({ error: err.code });
        });
};

exports.deleteTodo = (req, res) => {
    const document = db.doc(`/todos/${req.params.todoId}`);

    document
        .get()
        .then(doc => {
            if (!doc.exists) {
                return res.status(404)
                    .json({ error: `Todo ${req.params.todoId} not found` });
            } else {
                return document.delete();
            }
        })
        .then(() => {
            res.json({ message: `Todo ${req.params.projectId} deleted successfully` });
        })
        .catch((err) => {
            res.status(500).json({ error: err.code });
            console.error(err);
        });
};