const { db } = require('../util/admin');

exports.getAllProjects = (req, res) => {
    db
        .collection('projects')
        .orderBy('createdAt', 'desc')
        .get()
        .then(data => {
            let projects = [];
            data.forEach(doc => {
                projects.push({
                    projectId: doc.id,
                    name: doc.data().name,
                    createdAt: doc.data().createdAt
                });
            });
            return res.json(projects);
        })
        .catch((err) => {
            res.status(500).json({ error: err.code });
            console.error(err);
        });
};

exports.addNewProject = (req, res) => {
    if (req.body.name === '') {
        return res.status(400).json({
            name: 'Must not be empty'
        });
    }
    
    const newProject = {
        name: req.body.name,
        createdAt: new Date().toISOString()
    }
    
    db
        .collection('projects')
        .add(newProject)
        .then((doc) => {
            res.json({ message: `Document ${doc.id} created successfully` });
        })
        .catch((err) => {
            res.status(500).json({ error: err.code });
            console.error(err);
        });
};

exports.editProject = (req, res) => {
    const document = db.doc(`/projects/${req.params.projectId}`);

    const editProject = {
        name: req.body.name
    }

    if (editProject == '') {
        res.status(400).json({ error: 'Must not be empty' });
    }

    document
        .get()
        .then(doc => {
            if (!doc.exists) {
                return res.status(404)
                    .json({ error: `Project ${req.params.projectId} not found` });
            } else {
                return document.update(editProject);
            }
        })
        .then(() => {
            res.json({ message: `Project ${req.params.projectId} edited successfully` })
        })
        .catch((err) => {
            console.error(err);
            return res.status(500).json({ error: err.code });
        });
};

exports.deleteProject = (req, res) => {
    const document = db.doc(`/projects/${req.params.projectId}`);

    document
        .get()
        .then(doc => {
            if (!doc.exists) {
                return res.status(404)
                    .json({ error: `Project ${req.params.projectId} not found` });
            } else {
                return document.delete();
            }
        })
        .then(() => {
            res.json({ message: `Project ${req.params.projectId} deleted successfully` });
        })
        .catch((err) => {
            res.status(500).json({ error: err.code });
            console.error(err);
        });
};