const functions = require('firebase-functions');
const express = require('express');
const app = express();

const { getAllProjects, addNewProject, deleteProject, editProject } = require('./routes/projects');
const { getTodoFromProject, deleteTodo, editTodo } = require('./routes/tasks');

// Projects routes
app.get('/projects', getAllProjects);
app.post('/project', addNewProject);
app.post('/project/:projectId', editProject);
app.delete('/project/:projectId', deleteProject);

// Tasks routes
app.get('/todos/:projectId', getTodoFromProject);
//TODO: Create route task from project
app.post('/todo/:todoId', editTodo);
app.delete('/todo/:todoId', deleteTodo);
//TODO: Delete route all task

exports.api = functions.https.onRequest(app);
